const Kafka = require('node-rdkafka')
    , DefaultTopic = require('./DefaultTopic')


class Producer {
    constructor (config, defaultTopic = DefaultTopic) {
        let producer = new Kafka.Producer(config, {});

// Connect to the broker manually
        producer.connect(null, (err, metadata) => {
            console.log(metadata);
            console.error(err);
        });

        producer.on('ready', () => {
            console.log('Kafka producer is ready')
        })

        producer.on('event.error', function(err) {
            console.error('Error from producer');
            console.error(err);
        })

        // Poll for events every 100 ms
        producer.setPollInterval(100);

        producer.on('delivery-report', function(err, report) {
            // Report of delivery statistics here:
            //
            console.log(report);
        });


        this.producer = producer;
        this.defaultTopic = defaultTopic;
    }

    sendEvent (event, topic) {

        let actualTopic = topic || this.defaultTopic;

        console.log('Publishing event to topic ' + actualTopic)

        this.producer.produce(
            // Topic to send the message to
            actualTopic,
            // optionally we can manually specify a partition for the message
            // this defaults to -1 - which will use librdkafka's default partitioner (consistent random for keyed messages, random for unkeyed messages)
            null,
            // Message to send. Must be a buffer
            new Buffer(JSON.stringify(event)),
            // for keyed messages, we also specify the key - note that this field is optional
            null,
            // you can send a timestamp here. If your broker version supports it,
            // it will get added. Otherwise, we default to 0
            Date.now(),
            // you can send an opaque token here, which gets passed along
            // to your delivery reports
        );
    }
}

module.exports = Producer;