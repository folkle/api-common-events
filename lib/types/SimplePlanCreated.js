const Event = require('./Event')

class SimplePlanCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = SimplePlanCreated;
