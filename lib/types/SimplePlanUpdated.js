const Event = require('./Event')

class SimplePlanUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = SimplePlanUpdated;
