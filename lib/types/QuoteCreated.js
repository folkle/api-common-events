const Event = require('./Event')

class QuoteCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = QuoteCreated;
