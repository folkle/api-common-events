const Event = require('./Event')

class QuoteUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = QuoteUpdated;
