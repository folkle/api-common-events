const Event = require('./Event')

class SignUpVerified extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = SignUpVerified;
