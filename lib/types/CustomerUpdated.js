const Event = require('./Event')

class CustomerUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = CustomerUpdated;
