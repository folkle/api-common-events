const Event = require('./Event')

class SimplePlanImageSaved extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = SimplePlanImageSaved;
