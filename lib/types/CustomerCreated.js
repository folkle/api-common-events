const Event = require('./Event')

class CustomerCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = CustomerCreated;
