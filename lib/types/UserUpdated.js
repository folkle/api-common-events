const Event = require('./Event')

class UserUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = UserUpdated;
