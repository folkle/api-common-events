const Event = require('./Event')

class UserCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = UserCreated;
