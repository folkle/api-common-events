const Event = require('./Event')

class SignedUp extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = SignedUp;
