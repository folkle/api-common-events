const Event = require('./Event')

class PrototypeCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = PrototypeCreated;
