const Event = require('./Event')

class AccountUpdated extends Event {

    constructor(data) {
        super(data)
    }

}

module.exports = AccountUpdated;
