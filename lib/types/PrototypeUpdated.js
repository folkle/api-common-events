const Event = require('./Event')

class PrototypeUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = PrototypeUpdated;
