const Event = require('./Event')

class ForgotPassword extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = ForgotPassword;
