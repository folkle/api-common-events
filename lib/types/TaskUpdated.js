const Event = require('./Event')

class TaskUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = TaskUpdated;
