const Event = require('./Event')

class UserVerified extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = UserVerified;
