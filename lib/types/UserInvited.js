const Event = require('./Event')

class UserInvited extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = UserInvited;
