const Event = require('./Event')

class ConsumableUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = ConsumableUpdated;
