const Event = require('./Event')

class TaskCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = TaskCreated;
