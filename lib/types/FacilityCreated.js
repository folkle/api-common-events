const Event = require('./Event')

class FacilityCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = FacilityCreated;
