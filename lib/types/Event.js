const uuid = require('uuid/v4')

class Event {

    constructor (data) {
        this.type = this.constructor.name
        this.id = uuid()
        this.timestamp = new Date()
        this.data = data;
    }

}


module.exports = Event;