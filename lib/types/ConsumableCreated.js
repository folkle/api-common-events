const Event = require('./Event')

class ConsumableCreated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = ConsumableCreated;
