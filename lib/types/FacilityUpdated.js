const Event = require('./Event')

class FacilityUpdated extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = FacilityUpdated;
