const Event = require('./Event')

class FacilityDeleted extends Event {

    constructor(data) {
        super(data)
    }
}

module.exports = FacilityDeleted;
