const Kafka = require('node-rdkafka')
    , DefaultTopic = require('./DefaultTopic')
    , EventEmitter = require('events');

console.log(Kafka.features);
console.log(Kafka.librdkafkaVersion);


module.exports.DefaultTopic = DefaultTopic;
module.exports.types = require('./types')
module.exports.Producer = require('./Producer')
module.exports.EventHandler = require('./EventHandler')


module.exports.getConsumer = (config, groupId, topics = [DefaultTopic]) => {
    var consumer = new Kafka.KafkaConsumer({
        'group.id': groupId,
        'metadata.broker.list': config["metadata.broker.list"],
        'offset_commit_cb': function(err, topicPartitions) {

            if (err) {
                // There was an error committing
                console.error(err);
            } else {
                // Commit went through. Let's log the topic partitions
                console.log(topicPartitions);
            }

        }
    })

    consumer.connect();

    consumer
        .on('ready', function() {
            console.log('Consumer ready. Subscribing to topics: ', topics)
            consumer.subscribe(topics);

            // Consume from the librdtesting-01 topic. This is what determines
            // the mode we are running in. By not specifying a callback (or specifying
            // only a callback) we get messages as soon as they are available.
            consumer.consume();
        })

    return consumer;
}


module.exports.wireUpHandlers = (consumer, Handlers) => {
    let parsedRelay = new EventEmitter();

    consumer.on('data', (data) => {
        let message = data.value.toString();

        let event = JSON.parse(message);

        console.info('received event: \n', event.id);

        parsedRelay.emit('data', event);
    })

    let handlers = Handlers.map(Handler => new Handler(parsedRelay))

    return handlers;
}
