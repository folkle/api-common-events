

class EventHandler {

    constructor(consumer){
        this.consumer = consumer;

        console.info('Registered handler: ' + this.constructor.name);

        consumer.on('data', (event) => {

            if (!this.filter(event)) return;

            console.debug('handling: \n', event);

            this.delegate(event);
        })
    }

    filter(event) {
        return true
    }

    delegate(event) {
        throw new Error("Delegate not implemented")
    }
}

module.exports = EventHandler;